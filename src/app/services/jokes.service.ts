import { Injectable } from '@angular/core';
import { Joke } from '../models/Joke';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class JokesService {
  private jokeUrl: string;

  constructor(private http: HttpClient) {

  }

  getJoke() {
    this.jokeUrl = 'https://official-joke-api.appspot.com/jokes/programming/random';
    return this.http.get<Joke>(this.jokeUrl);
  }
}
