export interface Student {
    firstName: string,
    middleIni: string,
    lastName: string,
    date: any,
    // address: {
    //     street: string,
    //     apartNum?: string,
    //     city: string,
    //     state: string,
    //     zip: number
    // },
    phone: number,
    email: string,
    courseLocation: string,
    // cohort?: {
    //     start: string,
    //     end: string,
    //     inclusive: string
    // },
    ssn: number,
    birthdate: any,
    // usCitizen?: {
    //     authorized: any,
    //     ifNo: any
    // },
    emercencyName: string,
    emergencyNum: number
}