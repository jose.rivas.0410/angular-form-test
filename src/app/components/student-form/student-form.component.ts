import { Component, OnInit, ViewChild } from '@angular/core';
import { EmailValidator, Validators } from '@angular/forms';
// import SignaturePad from 'signature_pad';
import { Joke } from '../../models/Joke';
import { JokesService } from '../../services/jokes.service'

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  joke:Joke;

  // @ViewChild('sPad', {static: true}) signaturePadElement;
  // signaturePad: any;

  firstName:string;
  middleIni: string;
  lastName: string;
  date:any;
  street:string;
  apartNum:number;
  city:string;
  state:string;
  zip:number;
  phone:any;
  email:string;
  course:string;
  cl1:any;
  cl2:any;
  cl3:any;
  cl4:any;
  personal_ssn:any;
  dob:any;
  emerg_name:string;
  emerg_num:number;
  service_start:any;
  service_end:any;
  sign:string;
  date_of_sign:any;
  rank_fillout:string;
  citizen:any;

  showForm:boolean;
  showForm2:boolean;
  showForm3:boolean;
  showForm4:boolean;
  showForm5:boolean;
  showForm6:boolean;
  showForm7:boolean;
  showForm8:boolean;
  showForm9:boolean;
  touched:boolean;

  maxDate="2030-12-31";
  minDate='2020-01-28';

  maxDate2="2020-12-31";
  minDate2='1945-01-01';

  maxDate3="2030-12-31";
  minDate3='1945-01-01';

  maxDate4="2030-12-31";
  minDate4='2018-01-01';

  OnlyNumbersAllowed(event):boolean {
    const charCode = (event.which)?event.which: event.keycode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  OnlyLettersAllowed(event):boolean {
    const charCode = (event.which)?event.which: event.keycode;
    if (charCode > 31 && (charCode < 65 || charCode > 122)) {
      return false;
    }
    return true;
  }

  constructor(private jokeService: JokesService) { }

  ngOnInit(): void {
    this.jokeService.getJoke().subscribe((data) => {
      this.joke = data;
  });
  }

  // ngAfterViewInit(): void {
  //   this.signaturePad = new SignaturePad(this.signaturePadElement.nativeElement);
  // }

  // Methods
  toggleForm() {
    this.showForm = !this.showForm
  }

  toggleForm2() {
    this.showForm2 = !this.showForm2
  }

  toggleForm3() {
    this.showForm3 = !this.showForm3
  }

  toggleForm4() {
    this.showForm4 = !this.showForm4
  }

  toggleForm5() {
    this.showForm5 = !this.showForm5
  }

  toggleForm6() {
    this.showForm6 = !this.showForm6
  }

  toggleForm7() {
    this.showForm7 = !this.showForm7
  }

  toggleForm8() {
    this.showForm8 = !this.showForm8
  }

  toggleForm9() {
    this.showForm9 = !this.showForm9
  }

  // clear() {
  //   this.signaturePad.clear();
  // }

  // undo() {
  //   const data = this.signaturePad.toData();
  //   if (data) {
  //     data.pop(); // remove the last dot or line
  //     this.signaturePad.fromData(data);
  //   }
  // }

  // changeColor() {
  //   const r = Math.round(Math.random() * 255);
  //   const g = Math.round(Math.random() * 255);
  //   const b = Math.round(Math.random() * 255);
  //   const color = 'rgb(' + r + ',' + g + ',' + b + ')';
  //   this.signaturePad.penColor = color;
  // }

}
